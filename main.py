from flask import Flask, g, request, jsonify, json

from functools import reduce
import logging
from netaddr import IPAddress
import geoip2.database
import geoip2.errors

# configuration
DB_FILE_LOCATION = '/srv/geodb/GeoLite2-City.mmdb'
host = '127.0.0.1'
port = 8000
debug = True
verify_string = 'T2ggC3c6qaAvFfcM'

#internal variables
JSON_MAPPING = {
        'country_name': 'country.name',
        'longitude': 'location.longitude',
        'zip_code': 'postal.code',
        'time_zone': 'location.time_zone',
        'region_code': 'subdivisions.most_specific.iso_code',
        'country_code': 'country.iso_code',
        'latitude': 'location.latitude',
        'city': 'city.name',
        'region_name': 'subdivisions.most_specific.name'
    }

privacy_headers = [
        'VIA',
        'X-FORWARDED-FOR',
        'X-FORWARDED',
        'X-Real-IP',
        'FORWARDED-FOR',
        'FORWARDED-FOR-IP',
        'FORWARDED',
        'CLIENT-IP',
        'PROXY-CONNECTION',
        'PROXY-PROTOCOL',
        'PROXY_PROTOCOL'
    ]

ValidIpAddressRegex = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";
ValidHostnameRegex = "^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$";


app = Flask(__name__)
app.json.sort_keys = False


def setup_logging(loglevel='INFO'):
    logformat = "%(asctime)s: %(message)s"
    if loglevel:
        logging.basicConfig(level=logging.DEBUG,format=logformat)
    else:
        logging.basicConfig(level=logging.INFO,format=logformat)


def get_db_reader():
    reader = getattr(g, '_db_reader', None)
    if reader is None:
        app.logger.info("opening connection to database")
        reader = geoip2.database.Reader(DB_FILE_LOCATION)
    return reader


@app.route("/")
def ip_info():
    chain = []
    ip_chain = {}
    response = {}
    if not request.environ.get('HTTP_C_REAL_IP') is None:
        chain.append(request.environ['HTTP_C_REAL_IP'])

    if request.environ.get('HTTP_X_FORWARDED_FOR') is None:
        if not IPAddress(request.environ['REMOTE_ADDR']).is_loopback():
            chain.append(request.environ['REMOTE_ADDR'])
    else:
        for ip in request.environ['HTTP_X_FORWARDED_FOR'].split(','):
            chain.append(ip.strip(' '))

    for ip in chain:
        ipinfo = {}
        if not IPAddress(ip).is_private():
            try:
                app.logger.info("looking up IP address: {}".format(ip))
                geoip_reader = get_db_reader()
                result = geoip_reader.city(ip)
                for key, value in JSON_MAPPING.items():
                    try:
                        ipinfo[key] = reduce(getattr, value.split('.'), result)
                    except AttributeError:
                        ipinfo[key] = ''
                ipinfo['ip'] = ip
            except geoip2.errors.AddressNotFoundError as e:
                app.logger.warning("Unable find ip address: {}".format(e))
                try:
                    ipinfo['error'] = e.message
                except AttributeError as ae:
                    ipinfo['error'] = str(e)
        else:
            ipinfo['ip'] = ip
            ipinfo["country_name"] = None
            ipinfo["longitude"] = None
            ipinfo["zip_code"] = None
            ipinfo["time_zone"] = None
            ipinfo["region_code"] = None
            ipinfo["country_code"] = None
            ipinfo["latitude"] = None
            ipinfo["city"] = None
            ipinfo["region_name"] = None

        ip_chain[ip] = ipinfo

    response['ip'] = ip_chain
    headers = {}
    for header in privacy_headers:
        headers[header] = request.headers.get(header)
    response['headers'] = headers
    response['User-Agent'] = request.headers.get('User-Agent')
    response['verification_string'] = verify_string
    app.logger.info("returning response: \n{}".format(json.dumps(response, separators=(',', ': '))))
    return jsonify(response)


if __name__ == '__main__':
    setup_logging()
    app.run(debug=debug, host=host, port=port)
