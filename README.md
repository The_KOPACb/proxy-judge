# Proxy Judge

Backend for judging proxy

service returns json with basic info about headers and ip address of client

example:

```
{
  "ip": {
    "REDACTED": {
      "country_name": "Czechia",
      "longitude": 14.4124,
      "zip_code": "110 00",
      "time_zone": "Europe/Prague",
      "region_code": "10",
      "country_code": "CZ",
      "latitude": 50.0883,
      "city": "Prague",
      "region_name": "Prague",
      "ip": "REDACTED"
    }
  },
  "headers": {
    "VIA": null,
    "X-FORWARDED-FOR": null,
    "X-FORWARDED": null,
    "X-Real-IP": null,
    "FORWARDED-FOR": null,
    "FORWARDED-FOR-IP": null,
    "FORWARDED": null,
    "CLIENT-IP": null,
    "PROXY-CONNECTION": null,
    "PROXY-PROTOCOL": null,
    "PROXY_PROTOCOL": null
  }
}

```

nginx location:

```
    location / {
      proxy_pass http://ip:port;
      proxy_pass_request_headers      on;
      proxy_set_header Host $host;
      proxy_set_header C-Real-IP $remote_addr;
    }
```


demo available on https://pjudge.dev-ops.zone/